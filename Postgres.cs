using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace Markov
{
    class PostgresMapper : IDatabase
    {
        private string connString;

        public static string HostStringFromParts(string host, ushort port, string username, string password, string? database)
        {
            database = database == null ? database : username;
            return $"Host={host};Port={port};Database={database};Username={username};Password={password}";
        }

        public PostgresMapper(string connString)
        {
            this.connString = connString;
        }

        protected async Task<NpgsqlConnection> OpenAsync()
        {
            var conn = new NpgsqlConnection(connString);
            await conn.OpenAsync();
            return conn;
        }

        public async Task PushAsync(string first, string second, string? third, ulong? user, ulong? guild)
        {
            await using var conn = await OpenAsync();

            using (var cmd = conn.CreateCommand(@"
                INSERT INTO pairs (fst, snd, thd, _user, guild)
                VALUES (@fst, @snd, @thd, @user, @guild)
                ON CONFLICT DO NOTHING"))
            {
                cmd.Parameters.Add("fst", NpgsqlDbType.Text);
                cmd.Parameters.Add("snd", NpgsqlDbType.Text);
                cmd.Parameters.Add("thd", NpgsqlDbType.Text);
                cmd.Parameters.Add("user", NpgsqlDbType.Bigint);
                cmd.Parameters.Add("guild", NpgsqlDbType.Bigint);
                await cmd.PrepareAsync();
                cmd.Parameters[0].Value = first;
                cmd.Parameters[1].Value = second;

                if (third != null && third.Length != 0)
                {
                    cmd.Parameters[2].Value = third;
                }
                else
                {
                    cmd.Parameters[2].Value = DBNull.Value;
                }

                if (user is ulong && guild is ulong)
                {
                    cmd.Parameters[3].Value = user;
                    cmd.Parameters[4].Value = guild;
                }
                else
                {
                    cmd.Parameters[3].Value = DBNull.Value;
                    cmd.Parameters[4].Value = DBNull.Value;
                }

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<(string, string, string)> FirstAsync(ulong? user, ulong? guild)
        {
            if (user is ulong && guild == null || guild is ulong && user == null)
            {
                throw new InvalidOperationException("user and guild must either both be set or not at all");
            }

            await using var conn = await OpenAsync();

            using (var cmd = conn.CreateCommand())
            {

                if (user is ulong && guild is ulong)
                {
                    cmd.CommandText = "SELECT first_by_user(@user, @guild)";
                    cmd.Parameters.Add("user", NpgsqlDbType.Bigint);
                    cmd.Parameters.Add("guild", NpgsqlDbType.Bigint);
                    await cmd.PrepareAsync();
                    cmd.Parameters[0].Value = user;
                    cmd.Parameters[1].Value = guild;
                }
                else
                {
                    cmd.CommandText = @"
                    SELECT fst, snd, thd
                    FROM pairs
                    TABLESAMPLE SYSTEM_ROWS(1)";
                    await cmd.PrepareAsync();
                }
                await using var reader = await cmd.ExecuteReaderAsync();
                await reader.ReadAsync();
                var first = reader.GetString(0);
                var second = reader.GetString(1);
                var third = reader.GetString(2);
                if (third != null)
                    return (first, second, third);
                else
                    return await FirstAsync(user, guild);
            }
        }

        public async Task<string?> ThirdAsync(string first, string second, ulong? user, ulong? guild)
        {
            if (user is ulong && guild == null || guild is ulong && user == null)
            {
                throw new InvalidOperationException("user and guild must either both be set or not at all");
            }

            await using var conn = await OpenAsync();

            using (var cmd = conn.CreateCommand())
            {
                if (user is ulong && guild is ulong)
                {
                    cmd.CommandText = "SELECT third_by_user(@fst, @snd, @user, @guild)";
                    cmd.Parameters.Add("fst", NpgsqlDbType.Text);
                    cmd.Parameters.Add("snd", NpgsqlDbType.Text);
                    cmd.Parameters.Add("user", NpgsqlDbType.Bigint);
                    cmd.Parameters.Add("guild", NpgsqlDbType.Bigint);
                    await cmd.PrepareAsync();
                }
                else
                {
                    cmd.CommandText = "SELECT third(@fst, @snd)";
                    cmd.Parameters.Add("fst", NpgsqlDbType.Text);
                    cmd.Parameters.Add("snd", NpgsqlDbType.Text);
                    await cmd.PrepareAsync();
                }
                cmd.Parameters[0].Value = first;
                cmd.Parameters[1].Value = second;
                if (user is ulong && guild is ulong)
                {
                    cmd.Parameters[2].Value = user;
                    cmd.Parameters[3].Value = guild;
                }
                var third = await cmd.ExecuteScalarAsync();
                if (third is DBNull)
                    return null;
                else
                    return (string)third;
            }
        }
    }
}