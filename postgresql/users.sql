CREATE TABLE users (
    id bigint not null,
    guild bigint not null,
    PRIMARY KEY (id, guild)
);