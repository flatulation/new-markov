CREATE TABLE pairs (
  fst text not null,
  snd text not null,
  thd text,
  _user bigint,
  guild bigint,
  UNIQUE (fst, snd, thd, _user, guild),
  FOREIGN KEY (_user, guild) REFERENCES users (id, guild)
);