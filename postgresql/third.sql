CREATE FUNCTION third(fst text, snd text) RETURNS text AS $$ 
if (!(fst && snd)) {
  plv8.elog(ERROR, "fst or snd cannot be null");
  return null;
} 
const thirds = plv8.execute("SELECT thd FROM pairs WHERE fst = $1 AND snd = $2", [fst, snd]).map(row => row.thd);
const idx = Math.floor(Math.random() * thirds.length);
return thirds[idx];
$$ LANGUAGE plv8 STRICT PARALLEL RESTRICTED

CREATE FUNCTION third_by_user(fst text, snd text, _user bigint, guild bigint) RETURNS text AS $$ 
if (!(_user && guild && fst && snd)) {
  plv8.elog(ERROR, "ALL arguments of this function must be set");
  return null;
}
const thirds = plv8.execute("SELECT thd FROM pairs WHERE fst = $1 AND snd = $2 AND _user = $3 AND guild = $4", [fst, snd, _user, guild]).map(row => row.thd);
const idx = Math.floor(Math.random() * thirds.length);
return thirds[idx];
$$ LANGUAGE plv8 STRICT PARALLEL RESTRICTED