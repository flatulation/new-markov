CREATE FUNCTION finish() RETURNS text AS $$ 
const pairs = plv8.execute("SELECT fst, snd FROM pairs WHERE thd IS NULL");
const idx = Math.floor(Math.random() * pairs.length);
const {fst, snd} = pairs[idx];
return `${fst} ${snd}`;
$$ LANGUAGE plv8 PARALLEL RESTRICTED