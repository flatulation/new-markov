CREATE TYPE triplet AS (fst text, snd text, thd text);

CREATE OR REPLACE FUNCTION first_by_user(_user bigint, guild bigint) RETURNS triplet AS $$
if (!(_user && guild)) {
  plv8.elog(ERROR, "_user and guild must either both be set or not at all");
  return;
}
const pairs = plv8.execute("SELECT fst, snd, thd FROM pairs WHERE _user = $1 AND guild = $2", [_user, guild]);
const idx = Math.floor(Math.random() * pairs.length);
plv8.elog(WARNING, typeof pairs[idx]);
return pairs[idx];
$$ LANGUAGE plv8 STRICT PARALLEL RESTRICTED;