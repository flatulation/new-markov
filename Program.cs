using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using DSharpPlus;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Markov
{
    static class Program
    {
#nullable disable
        static public IConfiguration Configuration { get; private set; }
#nullable restore

        static private readonly IReadOnlyDictionary<string, string> DefaultConfiguration = new Dictionary<string, string>
        {
            { "postgres:host", "localhost" },
            { "postgres:port", "5432" },
            { "postgres:database", "postgres" },
            { "postgres:username", "postgres" },
            { "postgres:password", "postgres" },
            { "discord:perms", ((ulong)(Permissions.EmbedLinks |
                Permissions.ReadMessageHistory |
                Permissions.AccessChannels |
                Permissions.SendMessages)).ToString() }
        };

        static async Task Main(string[] args)
        {
            var proc = Process.GetCurrentProcess();
            var ts = DateTime.UtcNow.Subtract(proc.StartTime);
#pragma warning disable CS4014
            Console.Out.WriteLineAsync($"{ts.Milliseconds} msec to start process");
#pragma warning restore CS4014

            Configuration = new ConfigurationBuilder()
                .SetBasePath(System.Environment.CurrentDirectory)
                .AddInMemoryCollection(DefaultConfiguration)
                .AddIniFile("config.ini", optional: false, reloadOnChange: false)
                .AddEnvironmentVariables("MARKOV_")
                .AddCommandLine(args)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console()
                .CreateLogger();

            var pgConfig = Configuration.GetSection("postgres");
            var connString = PostgresMapper.HostStringFromParts(
                pgConfig["host"],
                ushort.Parse(pgConfig["port"]),
                pgConfig["username"],
                pgConfig["password"],
                pgConfig["database"]
            );
            var chain = new Chain(new PostgresMapper(connString));

            var discordConfig = Configuration.GetSection("discord");
            var bot = new Bot(discordConfig["token"], chain);
            await bot.ConnectAsync();
            var perms = discordConfig["perms"];
            Log.Information($"https://discordapp.com/oauth2/authorize?client_id={bot.Id}&scope=bot&permissions={perms}");
            await Task.Delay(-1);
        }
    }
}