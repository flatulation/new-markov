using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markov
{
    class Chain
    {
        private IDatabase database;

        public Chain(IDatabase data)
        {
            this.database = data;
        }

        public async Task TrainAsync(string text, ulong? user, ulong? guild)
        {
            text = text.Replace("\n", " ");
            var queue = new Queue<string>(text.Split(' '));
            if (queue.Count <= 3)
            {
                return;
            }

            string first = queue.Dequeue();
            string second = queue.Dequeue();
            string third;
            while (true)
            {
                try
                {
                    third = queue.Dequeue();
                    await database.PushAsync(first, second, third, user, guild);
                }
                catch (InvalidOperationException)
                {
                    await database.PushAsync(first, second, null, user, guild);
                    return;
                }
                first = second;
                second = third;
            }

        }

        public async Task<string> SpeakAsync(ulong? user, ulong? guild, ushort length = 2000)
        {
            string first, second;
            string? third;
            (first, second, third) = await database.FirstAsync(user, guild);
            string buffer = $"{first} {second} {third}";
            while (buffer.Length < length)
            {
                first = second;
                second = third;
                third = await database.ThirdAsync(first, second, user, guild);
                if (third == null)
                {
                    return buffer;
                }
                buffer += " " + third;
            }
            return buffer;
        }
    }
}