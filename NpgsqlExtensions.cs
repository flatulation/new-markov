using System.Threading.Tasks;
using Npgsql;

namespace Markov
{
    static class NpgsqlExtensions
    {
        public static NpgsqlCommand CreateCommand(this NpgsqlConnection conn, string sql)
        {
            return new NpgsqlCommand(sql, conn);
        }
    }
}