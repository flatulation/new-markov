using System.Threading.Tasks;

interface IDatabase
{
    Task PushAsync(string first, string second, string? third, ulong? user, ulong? guild);
    Task PushAsync(string first, string second, string? third) => PushAsync(first, second, third, null, null);
    Task<(string, string, string)> FirstAsync(ulong? user, ulong? guild);
    Task<(string, string, string)> FirstAsync() => FirstAsync(null, null);
    Task<string?> ThirdAsync(string first, string second, ulong? user, ulong? guild);
    Task<string?> ThirdAsync(string first, string second) => ThirdAsync(first, second, null, null);
}