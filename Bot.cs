using System;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using Serilog;
using Serilog.Events;

namespace Markov
{
    class Bot
    {
        private Chain Chain;
        private DiscordClient Discord;
        private CommandsNextModule Commands;

        public ulong Id
        {
            get
            {
                return Discord.CurrentUser.Id;
            }
        }

        public Bot(string token, Chain chain)
        {
            this.Chain = chain;

            Discord = new DiscordClient(new DiscordConfiguration
            {
                Token = token,
                TokenType = TokenType.Bot,
                LogLevel = LogLevel.Debug,
            });
            Discord.DebugLogger.LogMessageReceived += LogMessageRecieved;

            Commands = Discord.UseCommandsNext(new CommandsNextConfiguration { });
            Commands.RegisterCommands<Markov.Commands>();
        }

        public async Task ConnectAsync()
        {
            await Discord.ConnectAsync();
        }

        internal void LogMessageRecieved(object? sender, DebugLogMessageEventArgs e)
        {
            LogEventLevel serilogLevel;
            switch (e.Level)
            {
                case LogLevel.Critical:
                    serilogLevel = LogEventLevel.Fatal;
                    break;
                case LogLevel.Error:
                    serilogLevel = LogEventLevel.Error;
                    break;
                case LogLevel.Warning:
                    serilogLevel = LogEventLevel.Warning;
                    break;
                case LogLevel.Info:
                    serilogLevel = LogEventLevel.Information;
                    break;
                case LogLevel.Debug:
                    serilogLevel = LogEventLevel.Debug;
                    break;
                default:
                    throw new InvalidOperationException();
            }
            Log.Write(serilogLevel, "DSharpPlus {Application}: {Message}", e.Application, e.Message);
        }
    }
}